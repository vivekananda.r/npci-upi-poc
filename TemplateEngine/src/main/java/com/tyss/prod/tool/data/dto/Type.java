package com.tyss.prod.tool.data.dto;

import lombok.Data;

@Data
public class Type {
    private boolean enumList;
    private String name;
}
