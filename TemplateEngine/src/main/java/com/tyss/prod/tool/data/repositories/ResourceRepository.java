package com.tyss.prod.tool.data.repositories;

import com.tyss.prod.tool.data.db.entities.Resource;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ResourceRepository extends MongoRepository<Resource,String> {

    Resource findByName(String name);

    boolean existsByName(String name);
}
