package com.tyss.prod.tool.data.dto;

import lombok.Data;
import org.springframework.data.annotation.Id;

@Data
public class Rule {

    @Id
    private String id;
    private String name;
    private String description;
    private String expectedResult;
    private String actualResult;
}
