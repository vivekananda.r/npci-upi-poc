package com.tyss.prod.tool.data.dto;

import lombok.Data;

@Data
public class TemplateDTO {
    private String templateName;
    private Object template;
    private String xsdPath;
    private String xsdFileName;
}