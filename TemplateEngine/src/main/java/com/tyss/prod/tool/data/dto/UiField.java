package com.tyss.prod.tool.data.dto;

import lombok.Data;

import java.util.List;

@Data
public class UiField {
    private String fieldId;
    private String fieldLabel;
    private String fieldPlaceholder;
    private String fieldType;
    private String fieldValue;
    private List<String> fieldOptions;
}
