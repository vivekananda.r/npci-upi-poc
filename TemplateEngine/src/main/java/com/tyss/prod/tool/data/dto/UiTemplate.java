package com.tyss.prod.tool.data.dto;

import lombok.Data;

import java.util.List;
import java.util.Map;
import java.util.Set;

@Data
public class UiTemplate {
    private List<Section> sections;
}
