package com.tyss.prod.tool.data.dto;

import lombok.Data;

import java.util.List;
import java.util.Set;

@Data
public class Section {
    private String heading;
    private List<UiField> fields;
    private Set<Section> subSection;
}
