package com.tyss.prod.tool.data.db.entities;

import com.tyss.prod.tool.data.dto.Rule;
import lombok.Data;
import org.springframework.data.annotation.Id;

import java.util.List;

@Data
public class Template {
    @Id
    private String id;
    private String ui;
    private List<Rule> assertionRules;
    private String requestToUpi;
}
