package com.tyss.prod.tool.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.tyss.prod.tool.data.db.entities.RequestTemplate;
import com.tyss.prod.tool.data.dto.ApiResponse;
import com.tyss.prod.tool.data.dto.TemplateDTO;
import com.tyss.prod.tool.data.dto.UiTemplate;
import com.tyss.prod.tool.service.TemplateService;
import com.tyss.prod.tool.service.impl.UiService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;
import java.util.List;
import java.util.Map;
import java.util.Set;

@RestController
@RequestMapping("${template.controller.mapping}")
@Slf4j
@RequiredArgsConstructor
public class TemplateController {

    private final TemplateService templateService;

    //private final UiService uiService;

    @PostMapping(value = "upload/xsdfiles",consumes = {"multipart/form-data"})
    public ResponseEntity<String> uploadXsdFiles(@RequestParam List<MultipartFile> xsdFiles,
                                            @RequestParam String namespace,
                                            @RequestParam String localPart) throws FileNotFoundException, JsonProcessingException {

        return templateService.getJsonTemplateForXsdFiles(xsdFiles, namespace, localPart);
    }

    @PostMapping
    public ResponseEntity<ApiResponse> createTemplate(@RequestBody TemplateDTO templateDTO) {
        return templateService.saveTemplate(templateDTO);
    }

    @GetMapping("/{id}")
    public ResponseEntity<RequestTemplate> getTemplate(@PathVariable("id") String id) {
        return templateService.getTemplate(id);
    }

    @GetMapping
    public ResponseEntity<List<RequestTemplate>> getAllTemplate()
    {
        return templateService.getTemplates();
    }

    @PutMapping
    public ResponseEntity<String> editTemplate(@RequestBody String template) {
        return ResponseEntity.ok().body("Request template edited");
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteTemplate(@PathVariable String id)
    {
        return templateService.deleteTemplate(id);
    }

    //@GetMapping("/test")
    /*public Map<String, Set<String>> test(@RequestParam String commonsPath)
    {
       return uiService.test(commonsPath);
    }*/

	/*
	 * @PostMapping("/test2") public UiTemplate test2(@RequestParam String
	 * commonsPath) { return templateService.test(commonsPath); }
	 */

    @PostMapping("/testPost")
    public void testDisplay()
    {
        log.info("******************TESTING ***********************");
    }
}
