package com.tyss.prod.tool.errors.handler;

import com.tyss.prod.tool.data.dto.ApiResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.NoSuchElementException;

@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler(NoSuchElementException.class)
    public ResponseEntity<ApiResponse> handleNoSuchElementException(NoSuchElementException ex) {
        var apiResponse = new ApiResponse();
        apiResponse.setData(new HashMap<>());
        apiResponse.setResponseCode(HttpStatus.NOT_FOUND.value());
        apiResponse.setStatus(HttpStatus.NOT_FOUND);
        apiResponse.getData().put("errorMessage","Data Not Found for the given ID");
        return ResponseEntity.status(apiResponse.getResponseCode()).body(apiResponse);
    }

    @ExceptionHandler(FileNotFoundException.class)
    public ResponseEntity<ApiResponse> handleFileNotFoundException(FileNotFoundException ex) {
        var apiResponse = new ApiResponse();
        apiResponse.setData(new HashMap<>());
        apiResponse.setResponseCode(HttpStatus.NOT_FOUND.value());
        apiResponse.setStatus(HttpStatus.NOT_FOUND);
        apiResponse.getData().put("errorMessage","Given file not found in the path : "+ex.getMessage());
        return ResponseEntity.status(apiResponse.getResponseCode()).body(apiResponse);
    }

    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<ApiResponse> handleRuntimeException(RuntimeException ex) {

        log.info("Runtime exception "+ex);
        var apiResponse = new ApiResponse();
        apiResponse.setData(new HashMap<>());
        apiResponse.setResponseCode(HttpStatus.BAD_REQUEST.value());
        apiResponse.setStatus(HttpStatus.BAD_REQUEST);
        apiResponse.getData().put("Error : ",ex.getMessage());
        return ResponseEntity.status(apiResponse.getResponseCode()).body(apiResponse);
    }


}
