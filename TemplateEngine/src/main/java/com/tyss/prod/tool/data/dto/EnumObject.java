package com.tyss.prod.tool.data.dto;

import lombok.Data;

import java.util.Set;

@Data
public class EnumObject {
    private Set<String> values;
 }
