package com.tyss.prod.tool.service.impl;

import org.springframework.stereotype.Component;

import java.util.LinkedHashMap;
import java.util.Set;

@Component
public interface UiService {
    LinkedHashMap<String, Set<String>> test(String commonsPath);
}
