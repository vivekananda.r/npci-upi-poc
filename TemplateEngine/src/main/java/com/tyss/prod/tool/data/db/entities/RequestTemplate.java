package com.tyss.prod.tool.data.db.entities;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document("RequestTemplate")
public class RequestTemplate {
    @Id
    private String id;
    private String templateName;
    private Object template;
    private String xsdPath;
    private String xsdFileName;
}