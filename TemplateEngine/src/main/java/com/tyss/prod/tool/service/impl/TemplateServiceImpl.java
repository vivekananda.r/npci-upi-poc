package com.tyss.prod.tool.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

import javax.xml.namespace.QName;
/*import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;*/
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.lang3.StringUtils;
import org.apache.ws.commons.schema.XmlSchemaCollection;
import org.json.XML;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
/*import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;*/

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tyss.prod.tool.data.db.entities.RequestTemplate;
import com.tyss.prod.tool.data.db.entities.Resource;
import com.tyss.prod.tool.data.dto.ApiResponse;
import com.tyss.prod.tool.data.dto.Section;
import com.tyss.prod.tool.data.dto.TemplateDTO;
import com.tyss.prod.tool.data.dto.UiField;
import com.tyss.prod.tool.data.dto.UiTemplate;
import com.tyss.prod.tool.data.repositories.CommonsRepository;
import com.tyss.prod.tool.data.repositories.ResourceRepository;
import com.tyss.prod.tool.data.repositories.TemplateRepository;
import com.tyss.prod.tool.service.TemplateService;
import com.tyss.prod.tool.utils.ResponseUtils;
import com.tyss.prod.tool.xsd.utils.SchemaTypeXmlGenerator;
import com.tyss.prod.tool.xsd.utils.XmlGenOptions;

import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class TemplateServiceImpl implements TemplateService {

    private final TemplateRepository templatesRepository;

    private final HttpServletRequest request;

    private final ModelMapper modelMapper;

    private final ResourceRepository resourceRepository;

    private final CommonsRepository commonsRepository;

    UiTemplate template = new UiTemplate();

    String root = "";

    Map<String, List<String>> nodeAttributes = new HashMap<>();

    StringBuilder builder = new StringBuilder();

    @Override
    public ResponseEntity<RequestTemplate> getTemplate(String id) {
        log.info("ID : " + id);
        return ResponseEntity.ok().body(templatesRepository.findById(id).orElseThrow());
    }

    @Override
    public ResponseEntity<String> getJsonTemplate(String xsdPath, String nameSpace, String localPart) throws FileNotFoundException {
        SchemaTypeXmlGenerator generator = getXmlGenerator(xsdPath);
        String xml = generator.generateXml(new QName(nameSpace, localPart), true);

        return ResponseEntity.ok().body(XML.toJSONObject(xml).toString(4));
    }


    public ResponseEntity<String> getJsonTemplateForXsdFiles(List<MultipartFile> xsdFiles, String nameSpace, String localPart) throws FileNotFoundException, JsonProcessingException {

        var filePaths = new HashMap<String, String>();
        saveFilesToTempFolder(xsdFiles, filePaths);
        if (!filePaths.isEmpty()) {
            SchemaTypeXmlGenerator generator = getXmlGenerator(filePaths.get("xsd"));
            var xml = generator.generateXml(new QName(nameSpace, localPart), true);
            log.info(xml);

            ObjectMapper objectMapper = new ObjectMapper();
            Map<String, Object> jsonMap = objectMapper.readValue(XML.toJSONObject(xml).toString(4).replace("string value",""), new TypeReference<Map<String, Object>>(){});

            return ResponseEntity.ok().body(XML.toJSONObject(xml).toString(4).replace("string value",""));
        }
        return ResponseEntity.badRequest().body("Xsd file already exists");
    }

    private void saveFilesToTempFolder(List<MultipartFile> xsdFiles, Map<String, String> filePaths) {

        var filesToSave = checkIfXsdAlreadyExists(xsdFiles);

        if (!filesToSave.isEmpty()) {
            filesToSave.forEach(file -> {
                if (!file.isEmpty()) {
                    try {
                        String uploadsDir = "/xsd/";
                        String realPathToUploads = request.getServletContext().getRealPath(uploadsDir);
                        if (!new File(realPathToUploads).exists()) {
                            new File(realPathToUploads).mkdir();
                        }
                        String orgName = file.getOriginalFilename();
                        String filePath = realPathToUploads + orgName;
                        File dest = new File(filePath);
                        file.transferTo(dest);
                        if (!StringUtils.equalsIgnoreCase(file.getOriginalFilename(), "UPI-Common.xsd"))
                            filePaths.put("xsd", dest.getAbsolutePath());
                        else
                            filePaths.put("common", dest.getAbsolutePath());

                        saveXsdFilesToDb(dest);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    private void saveXsdFilesToDb(File dest) {

        var resource = new Resource();
        resource.setId(UUID.randomUUID().toString());
        resource.setPath(dest.getAbsolutePath());
        resource.setName(dest.getName());
        resourceRepository.save(resource);
    }

    private List<MultipartFile> checkIfXsdAlreadyExists(List<MultipartFile> xsdFiles) {
        var filesToSave = new ArrayList<MultipartFile>();
        xsdFiles.forEach(xsdFile -> {
            if (!resourceRepository.existsByName(xsdFile.getOriginalFilename())) {
                filesToSave.add(xsdFile);
            }
        });
        return filesToSave;
    }

    @Override
    public ResponseEntity<List<RequestTemplate>> getTemplates() {
        return ResponseEntity.ok().body(templatesRepository.findAll());
    }

    @Override
    public ResponseEntity<ApiResponse> saveTemplate(TemplateDTO templatedto) {
        var apiResponse = new ApiResponse();
        apiResponse.setData(new HashMap<>());

        var template = modelMapper.map(templatedto, RequestTemplate.class);

        if (!templatesRepository.existsByTemplateName(template.getTemplateName())) {
            template.setId(UUID.randomUUID().toString());
            log.info(template.toString());
            var xsd = resourceRepository.findByName(templatedto.getXsdFileName());
            template.setXsdPath(xsd.getPath());
            apiResponse.getData().put("successMessage", "template created successfully");
            templatesRepository.save(template);
            ResponseUtils.setOkResponse(apiResponse);
        } else {
            apiResponse.getData().put("errorMessage", "Template with name " + template.getTemplateName() + " already exists");
            ResponseUtils.setBadRequestResponse(apiResponse);
        }

        return ResponseEntity.status(apiResponse.getStatus()).body(apiResponse);
    }

    @Override
    public ResponseEntity<RequestTemplate> updateTemplate(RequestTemplate template) {
        var templateReq = templatesRepository.findById(template.getId()).orElseThrow();
        templateReq.setId(template.getId());
        return ResponseEntity.ok().body(templatesRepository.save(templateReq));
    }

    @Override
    public ResponseEntity<String> deleteTemplate(String id) {
        templatesRepository.deleteById(id);
        return ResponseEntity.ok().body("template deleted");
    }

    private SchemaTypeXmlGenerator getXmlGenerator(String xsdPath) throws FileNotFoundException {


        FileInputStream fis = new FileInputStream(xsdPath);
        XmlSchemaCollection coll = new XmlSchemaCollection();
        coll.setBaseUri(xsdPath);
        StreamSource source = new StreamSource(fis);
        coll.read(source);

        XmlGenOptions options = new XmlGenOptions();
        options.setMaxRecursiveDepth(1);
        options.setMaxRepeatingElements(2);

        return new SchemaTypeXmlGenerator(coll, options);
    }

    private void deleteFiles(Map<String, String> filePaths) {
        var keys = filePaths.keySet();
        keys.forEach(key -> {
            File dFileXsd = new File(filePaths.get(key));
            log.info(dFileXsd.getName() + " file deleted  : " + dFileXsd.delete());
        });
    }

	/*
	 * @Override public UiTemplate test(String commonsPath) {
	 * template.setSections(new ArrayList<>()); try { File file = new
	 * File(commonsPath); DocumentBuilder documentBuilder =
	 * DocumentBuilderFactory.newInstance().newDocumentBuilder(); Document document
	 * = documentBuilder.parse(file); if (document.hasChildNodes()) {
	 * getElements(document.getChildNodes()); } } catch (Exception e) {
	 * e.printStackTrace(); } buildTemplate();
	 * 
	 * return template; }
	 * 
	 * private void getElements(NodeList nodeList) { var curr = new ArrayList<>();
	 * for (int count = 0; count < nodeList.getLength(); count++) { Node elemNode =
	 * nodeList.item(count); if(!elemNode.getNodeName().contains("#")) {
	 * System.out.println(); System.out.println("<" + elemNode.getNodeName()+">"); }
	 * if (elemNode.getNodeType() == Node.ELEMENT_NODE) { Node node = null; if
	 * (elemNode.hasAttributes()) { NamedNodeMap nodeMap = elemNode.getAttributes();
	 * for (int i = 0; i < nodeMap.getLength(); i++) { node = nodeMap.item(i);
	 * if(!node.getNodeName().contains(":")) {
	 * System.out.println(node.getNodeName()); } if
	 * (StringUtils.length(node.getNodeName())!=0) { addRoot(node);
	 * addSubSections(node); if (i == 0) { curr.add(node.getNodeValue());
	 * nodeAttributes.put(node.getNodeValue(), new ArrayList<>() { }); } } else { if
	 * (!curr.isEmpty()) { nodeAttributes.get(curr.get(curr.size() -
	 * 1)).add(node.getNodeValue()); } } } }
	 * 
	 * 
	 * if (elemNode.hasChildNodes()) { getElements(elemNode.getChildNodes()); }
	 * 
	 * }
	 * 
	 * if(!elemNode.getNodeName().contains("#")){ System.out.println("<" +
	 * elemNode.getNodeName()+"/>"); System.out.println();} } }
	 * 
	 * private void addRoot(Node node) { if (template.getSections().isEmpty()) { var
	 * section = new Section(); section.setHeading(node.getNodeValue());
	 * section.setSubSection(new HashSet<>()); template.getSections().add(section);
	 * root = node.getNodeValue(); } }
	 * 
	 * private void addSubSections(Node node) { if (Objects.nonNull(root) &&
	 * !node.getNodeValue().equalsIgnoreCase(root)) { var subSection = new
	 * Section(); subSection.setHeading(node.getNodeValue());
	 * template.getSections().get(0).getSubSection().add(subSection); } }
	 * 
	 * private void buildTemplate() { template.getSections().forEach(section -> { if
	 * (Objects.nonNull(section.getSubSection())) {
	 * section.getSubSection().forEach(sub -> { if
	 * (Objects.nonNull(nodeAttributes.get(sub.getHeading())) &&
	 * !nodeAttributes.get(sub.getHeading()).isEmpty()) { sub.setFields(new
	 * ArrayList<>()); var n = nodeAttributes.get(sub.getHeading()); var temp =
	 * n.get(n.size() - 1); var fields =
	 * commonsRepository.findByName(temp.substring(temp.indexOf(":") + 1));
	 * fields.getValues().forEach(f -> { var field = new UiField();
	 * field.setFieldId(f); field.setFieldLabel(f.substring(f.indexOf(":") + 1));
	 * field.setFieldPlaceholder(f.substring(f.indexOf(":") + 1)); var options =
	 * commonsRepository.findByName(field.getFieldId()); if
	 * (Objects.nonNull(options)) { field.setFieldType("Dropdown");
	 * field.setFieldOptions(new ArrayList<>(options.getValues())); }
	 * field.setFieldType("Editable text"); sub.getFields().add(field); });
	 * 
	 * } }); } }); }
	 */
}