package com.tyss.prod.tool.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.tyss.prod.tool.data.db.entities.RequestTemplate;
import com.tyss.prod.tool.data.dto.ApiResponse;
import com.tyss.prod.tool.data.dto.TemplateDTO;
import com.tyss.prod.tool.data.dto.UiTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;
import java.util.List;

@Component
public interface TemplateService {

    ResponseEntity<RequestTemplate> getTemplate(String id);

    ResponseEntity<String> getJsonTemplate(String xsdPath, String nameSpace, String localPart) throws FileNotFoundException;

    ResponseEntity<String> getJsonTemplateForXsdFiles(List<MultipartFile> xsdFiles, String nameSpace, String localPart) throws FileNotFoundException, JsonProcessingException;
    ResponseEntity<List<RequestTemplate>> getTemplates();

    ResponseEntity<ApiResponse> saveTemplate(TemplateDTO templatedto);

    ResponseEntity<RequestTemplate> updateTemplate(RequestTemplate template);

    ResponseEntity<String> deleteTemplate(String id);

   // UiTemplate test(String commonsPath);
}
