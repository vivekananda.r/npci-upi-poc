package com.tyss.prod.tool.data.dto;

import lombok.Data;
import org.springframework.http.HttpStatus;

import java.util.Map;

@Data
public class ApiResponse {
    private int responseCode;
    private HttpStatus status;
    private Map<Object,Object> data;
}
