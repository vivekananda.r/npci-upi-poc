package com.tyss.prod.tool.data.dto;

import lombok.Data;

@Data
public class XsdUploadResponse {
   private String json;
   private String xml;
}
