package com.tyss.prod.tool.data.db.entities;

import lombok.Data;
import org.springframework.data.annotation.Id;

import java.util.Set;

@Data
public class Commons {
    @Id
    String id;
    String name;
    Set<String> values;
    String fileName;
}
