package com.tyss.prod.tool.data.db.entities;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document("Resources")
public class Resource {
    @Id
    private String id;
    private String path;
    private String name;
}
