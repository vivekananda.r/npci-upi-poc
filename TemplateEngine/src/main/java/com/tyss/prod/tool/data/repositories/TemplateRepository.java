package com.tyss.prod.tool.data.repositories;

import com.tyss.prod.tool.data.db.entities.RequestTemplate;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface TemplateRepository  extends MongoRepository<RequestTemplate,String> {
    boolean existsByTemplateName(String templateName);
}
