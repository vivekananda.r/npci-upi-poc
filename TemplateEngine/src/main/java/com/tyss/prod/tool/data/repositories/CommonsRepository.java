package com.tyss.prod.tool.data.repositories;

import com.tyss.prod.tool.data.db.entities.Commons;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommonsRepository extends MongoRepository<Commons, String> {

    Commons findByName(String name);

    boolean existsByNameAndFileName(String name, String fileName);
}