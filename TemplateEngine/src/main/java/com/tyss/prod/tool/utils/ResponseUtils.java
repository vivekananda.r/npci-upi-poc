package com.tyss.prod.tool.utils;

import com.tyss.prod.tool.data.dto.ApiResponse;
import org.springframework.http.HttpStatus;

public class ResponseUtils {

    public static void setOkResponse(ApiResponse apiResponse)
    {
        apiResponse.setResponseCode(HttpStatus.OK.value());
        apiResponse.setStatus(HttpStatus.OK);
    }

    public static void setBadRequestResponse(ApiResponse apiResponse)
    {
        apiResponse.setResponseCode(HttpStatus.BAD_REQUEST.value());
        apiResponse.setStatus(HttpStatus.BAD_REQUEST);
    }
}
